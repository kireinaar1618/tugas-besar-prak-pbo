# No.1 
**Use Case Table User Dana Yang Ada Dalam Program**
|No.|Use Case|Deskripsi|Nilai Prioritas|
|---|--------|---------|---------------|
|1.|Registrasi Pengguna|Pengguna melakukan registrasi dengan memasukkan username, password, dan nomor kontak. Sistem membuat objek User baru.|95|
|2.|Login|Pengguna melakukan login dengan memasukkan username dan password. Sistem memvalidasi kredensial dan melakukan login.|95|
|3.|Lihat Saldo|Pengguna dapat melihat saldo yang tersedia di akun mereka.|90|
|4.|Transfer Dana|Pengguna mentransfer dana dari akun mereka ke akun pengguna lain dengan memasukkan username penerima dan jumlah transfer.|90|
|5.|Cek Riwayat Transaksi|Pengguna dapat melihat riwayat transaksi yang telah dilakukan pada akun mereka.|85|
|6.|Tarik Tunai|Pengguna menarik tunai dari saldo akun mereka dengan memasukkan jumlah penarikan.|85|
|7.|Top Up|Pengguna menambahkan saldo ke akun mereka dengan melakukan top up atau pengisian ulang dana.|85|
|8.|Logout|Pengguna keluar dari Aplikasi Dana.|60|

**Use Case Table User Dana**

|No.|Use Case|Deskripsi|Nilai Prioritas|
|---|--------|---------|---------------|
|1.|Registrasi Pengguna|Pengguna melakukan registrasi dengan memasukkan username, password, dan nomor kontak. Sistem membuat objek User baru.|95|
|2.|Login|Pengguna melakukan login dengan memasukkan username dan password. Sistem memvalidasi kredensial dan melakukan login.|95|
|3.|Lihat Saldo|Pengguna dapat melihat saldo yang tersedia di akun mereka.|90|
|4.|Transfer Dana|Pengguna mentransfer dana dari akun mereka ke akun pengguna lain dengan memasukkan username penerima dan jumlah transfer.|90|
|5.|Cek Riwayat Transaksi|Pengguna dapat melihat riwayat transaksi yang telah dilakukan pada akun mereka.|85|
|6.|Tarik Tunai|Pengguna menarik tunai dari saldo akun mereka dengan memasukkan jumlah penarikan.|85|
|7.|Top Up|Pengguna menambahkan saldo ke akun mereka dengan melakukan top up atau pengisian ulang dana.|85|
|8.|Ubah Kata Sandi|Pengguna mengubah kata sandi akun mereka.|60|
|9.|Logout|Pengguna keluar dari Aplikasi Dana.|60|
|10.|Split Bill|Pengguna membagi biaya dengan teman atau keluarga saat melakukan pembayaran bersama-sama.|60|
|11.|Pembayaran Tagihan|Pengguna membayar tagihan rutin seperti tagihan listrik, tagihan telepon, dan tagihan internet melalui aplikasi Dana.|70|
|12.|Pembelian Produk/Jasa|Pengguna melakukan pembelian produk atau jasa melalui aplikasi Dana, seperti membeli tiket pesawat atau memesan makanan secara online.|75|
|13.|Donasi|Pengguna melakukan donasi kepada lembaga amal atau organisasi yang membutuhkan melalui aplikasi Dana.|80|
|14.|Pembelian Pulsa|Pengguna membeli pulsa telepon seluler melalui aplikasi Dana.|70|
|15.|Pengaturan Keamanan|Pengguna mengatur fitur keamanan tambahan seperti verifikasi dua faktor atau notifikasi transaksi melalui email atau SMS.|75|
|16.|Promosi dan Diskon|Pengguna melihat promosi dan diskon khusus untuk pengguna aplikasi Dana saat melakukan pembayaran atau transaksi.|80|
|17.|Pengaturan Preferensi Notifikasi|Pengguna mengatur preferensi notifikasi yang lebih rinci, seperti pemberitahuan transaksi, promo terbaru, atau pengumuman penting dari aplikasi Dana.|70|
|18.|Pembaruan Informasi Profil|Pengguna memperbarui informasi profil seperti alamat, email, atau nomor telepon yang terhubung dengan akun Dana mereka.|70|
|19.|Pemberitahuan Transaksi|Pengguna mengatur preferensi pemberitahuan untuk menerima notifikasi transaksi atau informasi penting terkait akun mereka.|80|
|20.|Layanan Pelanggan|Pengguna menghubungi layanan pelanggan melalui aplikasi Dana untuk mendapatkan bantuan atau melaporkan masalah terkait akun atau transaksi.|70|
|21.|Pengaturan Pengingat Pembayaran|Pengguna mengatur pengingat pembayaran untuk tagihan rutin seperti tagihan bulanan atau cicilan.|75|
|22.|Pembaruan Informasi Keuangan|Pengguna memperbarui informasi keuangan mereka, seperti rekening bank terkait atau metode pembayaran yang terhubung dengan akun Dana.|70|
|23.|Fitur Pencarian|Pengguna menggunakan fitur pencarian untuk mencari transaksi, kontak pengguna, atau riwayat transaksi berdasarkan kriteria tertentu.|75|
|24.|Fitur Favorit|Pengguna menandai kontak atau transaksi favorit mereka sebagai favorit untuk akses cepat dalam transaksi berikutnya.|80|
|25.|Pengaturan Privasi|Pengguna mengatur preferensi privasi mereka, seperti mengontrol visibilitas profil atau mengelola izin akses aplikasi Dana pada perangkat mereka.|75|
|26.|Fitur Pemindai QR Code|Pengguna menggunakan fitur pemindai QR code untuk melakukan pembayaran atau transfer dana dengan mudah dan cepat.|75|
|27.|Pengaturan Notifikasi|Pengguna mengatur preferensi notifikasi untuk menerima pemberitahuan transaksi atau informasi penting terkait akun mereka.|80|
|28.|Pemberian Kredit|Pengguna mengajukan pinjaman atau kredit melalui aplikasi Dana dengan persetujuan dan pencairan dana yang cepat.|75|
|29.|Program Cashback|Pengguna mendapatkan cashback saat melakukan transaksi dengan menggunakan aplikasi Dana.|70|
|30.|Iklan dan Promosi|Pengguna melihat iklan dan promosi dari bisnis yang bermitra dengan aplikasi Dana.|80|
|31.|Pembayaran Tagihan Bisnis|Pengguna membayar tagihan bisnis melalui aplikasi Dana.|75|
|32.|Program Loyalty|Pengguna mengumpulkan poin atau reward khusus dari bisnis melalui program loyalitas di aplikasi Dana.|70|
|33.|Penawaran Spesial|Pengguna mendapatkan penawaran dan diskon khusus saat melakukan pembelian dengan aplikasi Dana.|75|
|34.|Analisis Pengguna dan Pelaporan Keuangan|Pengguna melihat data pengguna dan laporan keuangan yang terperinci dalam aplikasi Dana.|75|
|35.|Penjualan dan Pemasaran Online|Pengguna melakukan pembelian produk atau layanan bisnis secara online melalui aplikasi Dana.|75|
|36.|Pembelian Investasi|Pengguna melakukan pembelian produk investasi, seperti saham atau reksa dana, melalui aplikasi Dana.|70|



**Use Case Table Manajemen Perusahaan Dana**

|No.|Use Case|Deskripsi|Nilai Prioritas|
|---|--------|---------|---------------|
|1.|Manajemen Pengguna|Admin dapat melihat, menambahkan, mengedit, atau menghapus data pengguna pada aplikasi Dana.|90|
|2.|Pengaturan Sistem Keamanan|Admin mengelola keamanan sistem, termasuk perlindungan data pengguna, pengawasan akses, dan pemantauan aktivitas tidak sah.|85|
|3.|Manajemen Transaksi|Admin dapat melihat, memantau, dan mengelola transaksi yang dilakukan oleh pengguna dalam aplikasi Dana.|85|
|4.|Manajemen Keamanan|Admin mengelola fitur keamanan seperti verifikasi dua faktor, mengatur aturan password, atau mengelola izin akses aplikasi Dana.|80|
|5.|Manajemen Promosi dan Diskon|Admin mengatur dan mengelola promosi, diskon, atau program cashback yang ditawarkan kepada pengguna.|75|
|6.|Manajemen Tagihan|Admin mengelola dan memantau pembayaran tagihan rutin yang dilakukan oleh pengguna melalui aplikasi Dana.|80|
|7.|Manajemen Pelanggan|Admin dapat melihat dan memantau data pelanggan, riwayat transaksi, atau kegiatan pengguna dalam aplikasi Dana.|85|
|8.|Analisis Data dan Laporan Keuangan|Admin menganalisis data pengguna, transaksi, dan keuangan dalam aplikasi Dana dan menghasilkan laporan keuangan atau analisis kinerja yang relevan.|90|
|9.|Pengaturan Sistem|Admin mengelola pengaturan sistem dan konfigurasi aplikasi Dana, seperti pengaturan mata uang, pengaturan server, atau integrasi pihak ketiga.|85|
|10.|Manajemen Layanan Pelanggan|Admin mengelola pertanyaan, permintaan, atau masalah pelanggan yang dikirim melalui layanan pelanggan dalam aplikasi Dana.|75|
|11.|Manajemen Program Kredit|Admin mengelola program kredit yang ditawarkan kepada pengguna, seperti menetapkan syarat-syarat pemberian kredit dan mengelola proses persetujuan pinjaman.|80|
|12.|Manajemen Program Loyalty|Admin mengatur dan mengelola program loyalitas yang ditawarkan kepada pengguna aplikasi Dana.|75|
|13.|Manajemen Integrasi Pihak Ketiga|Admin mengelola integrasi dengan pihak ketiga, seperti sistem pembayaran, platform pemasaran, atau layanan keuangan lainnya.|80|

**Use Case Table Direksi Perusahaan (dashboard, monitoring, analisis) Dana**

|No.|Use Case|Deskripsi|Nilai Prioritas|
|---|--------|---------|---------------|
|1.|Dashboard Pemantauan Kinerja|Direksi dapat melihat dashboard yang menyajikan informasi kinerja aplikasi Dana secara keseluruhan, seperti jumlah pengguna aktif, transaksi harian, pendapatan, dan metrik penting lainnya.|95|
|2.|Analisis Pengguna dan Segmen|Direksi dapat menganalisis profil dan karakteristik pengguna aplikasi Dana, termasuk demografi, perilaku pengguna, dan segmen pasar yang dijangkau.|90|
|3.|Analisis Transaksi dan Pendapatan|Direksi dapat menganalisis transaksi yang terjadi dalam aplikasi Dana, termasuk volume transaksi, nilai transaksi rata-rata, tren transaksi, serta melihat pendapatan yang dihasilkan oleh platform.|90|
|4.|Monitoring Keamanan dan Keandalan|Direksi dapat memonitor tingkat keamanan dan keandalan aplikasi Dana, termasuk deteksi ancaman keamanan, kejadian yang mencurigakan, serta memastikan performa aplikasi tetap optimal dan tersedia secara stabil.|85|
|5.|Monitoring Layanan Pelanggan|Direksi dapat memantau layanan pelanggan yang disediakan oleh aplikasi Dana, termasuk waktu tanggapan, tingkat kepuasan pelanggan, jumlah pengaduan, serta resolusi masalah yang dilakukan oleh tim dukungan pelanggan.|85|
|6.|Analisis Penggunaan Fitur dan Fungsi|Direksi dapat menganalisis penggunaan fitur dan fungsi dalam aplikasi Dana, termasuk fitur yang paling populer, tingkat adopsi, serta mengevaluasi efektivitas dan pengaruh fitur-fitur tersebut terhadap pengalaman pengguna.|80|
|7.|Monitoring Performa Teknis|Direksi dapat memantau performa teknis aplikasi Dana, termasuk waktu respons, kecepatan loading halaman, latensi, serta ketersediaan server dan infrastruktur teknologi yang mendukung aplikasi.|80|
|8.|Analisis Persaingan dan Tren Industri|Direksi dapat menganalisis persaingan dalam industri aplikasi keuangan digital, termasuk mengidentifikasi tren terkini, mengamati inovasi dari pesaing, serta mengidentifikasi peluang dan ancaman di pasar yang relevan.|75|
|9.|Analisis Retensi Pengguna|Direksi dapat menganalisis tingkat retensi pengguna aplikasi Dana, termasuk tingkat churn, jumlah pengguna yang kembali menggunakan aplikasi setelah periode tidak aktif, serta upaya untuk meningkatkan loyalitas dan retensi pengguna.|80|
|10.|Monitoring Kepuasan Pengguna|Direksi dapat memantau dan mengevaluasi tingkat kepuasan pengguna aplikasi Dana, termasuk melalui survei kepuasan pelanggan, ulasan pengguna, dan umpan balik dari berbagai saluran komunikasi untuk meningkatkan pengalaman pengguna.|85|
|11.|Analisis Keuangan Perusahaan|Direksi dapat menganalisis kinerja keuangan perusahaan yang terkait dengan aplikasi Dana, termasuk pendapatan, biaya operasional, laba bersih, dan margin keuntungan, serta membuat keputusan strategis berdasarkan analisis keuangan tersebut.|85|
|12.|Analisis Penetrasi Pasar dan Pertumbuhan|Direksi dapat menganalisis penetrasi pasar aplikasi Dana, termasuk market share, pertumbuhan pengguna, dan potensi ekspansi ke segmen pasar baru.|75|
|13.|Analisis Penggunaan Data dan Kebijakan|Direksi dapat menganalisis penggunaan data pengguna dalam aplikasi Dana dan memastikan kepatuhan terhadap kebijakan privasi dan regulasi terkait, serta melindungi integritas data pengguna dengan mengimplementasikan langkah-langkah keamanan yang tepat.|75|
|14.|Analisis Efisiensi Operasional|Direksi dapat menganalisis efisiensi operasional perusahaan terkait dengan aplikasi Dana, termasuk penggunaan sumber daya, produktivitas, dan efisiensi biaya, serta mengidentifikasi area yang perlu ditingkatkan untuk mengoptimalkan operasional perusahaan.|75|
|15.|Monitoring Kinerja Layanan Pihak Ketiga|Direksi dapat memantau dan mengevaluasi kinerja layanan pihak ketiga yang terintegrasi dengan aplikasi Dana, seperti penyedia layanan pembayaran, sistem keamanan, atau penyedia infrastruktur teknologi, untuk memastikan kualitas dan keandalan layanan tersebut.|85|
|16.|Analisis Riset Pasar dan Tren Konsumen|Direksi dapat menganalisis riset pasar dan tren konsumen terkait dengan industri aplikasi keuangan digital, termasuk mengidentifikasi peluang baru, tren penggunaan teknologi, serta mengevaluasi strategi pemasaran dan pengembangan produk yang relevan.|80|
|17.|Monitoring Inovasi dan Pengembangan Produk|Direksi dapat memonitor dan mengevaluasi upaya inovasi dan pengembangan produk dalam aplikasi Dana, termasuk melalui pengenalan fitur baru, integrasi dengan teknologi baru, dan pembaruan produk berdasarkan umpan balik pengguna dan tren industri.|80|


# No.2 
**Class Diagram Dana**

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UAS/2._Diagram_Class_Mermaid.js.png)


| No. | Aktor | Aksi                             | Sistem | Reaksi                                                                                                              |
|-----|-------|----------------------------------|--------|---------------------------------------------------------------------------------------------------------------------|
| 1.  | Aktor | Menjalankan program DanaApp      | Sistem | Menampilkan pesan selamat datang dan menu awal aplikasi Dana                                                         |
| 2.  | Aktor | Memilih menu "Register"          | Sistem | Meminta aktor untuk memasukkan username, password, dan nomor kontak, lalu melakukan registrasi akun baru            |
| 3.  | Aktor | Memasukkan informasi registrasi  | Sistem | Membuat objek User baru dengan informasi registrasi dan menambahkannya ke daftar pengguna                           |
| 4.  | Aktor | Memilih menu "Login"             | Sistem | Meminta aktor untuk memasukkan username dan password, lalu melakukan proses login                                    |
| 5.  | Aktor | Memasukkan informasi login       | Sistem | Memeriksa apakah kombinasi username dan password yang dimasukkan sesuai dengan akun yang terdaftar                   |
| 6.  | Sistem| Memeriksa kecocokan kombinasi    |        | - Jika cocok:                                    |   - Menyimpan pengguna yang sedang login sebagai pengguna saat ini                                                   |
|     |       | username dan password            |        |                                                  |   - Menampilkan pesan "Login berhasil!"                                                                          |
|     |       |                                  |        | - Jika tidak cocok:                               |   - Menampilkan pesan "Username atau password salah!"                                                             |
| 7.  | Aktor | Memilih menu "Keluar"            | Sistem | Menampilkan pesan terima kasih dan mengakhiri program                                                                 |
| 8.  | Aktor | Memilih menu "Lihat Saldo"       | Sistem | Menampilkan saldo pengguna saat ini                                                                                  |
| 9.  | Aktor | Memilih menu "Top Up"            | Sistem | Meminta aktor untuk memasukkan jumlah top-up, lalu menambahkan jumlah tersebut ke saldo pengguna                     |
| 10. | Aktor | Memasukkan jumlah top-up         | Sistem | Menambahkan jumlah top-up ke saldo pengguna dan memperbarui riwayat transaksi dengan informasi top-up                  |
| 11. | Aktor | Memilih menu "Tarik Tunai"       | Sistem | Meminta aktor untuk memasukkan jumlah penarikan tunai, lalu mengecek apakah saldo mencukupi untuk melakukan penarikan |
| 12. | Aktor | Memasukkan jumlah penarikan tunai| Sistem | Mengurangi jumlah penarikan tunai dari saldo pengguna, memperbarui riwayat transaksi dengan informasi penarikan tunai, dan menampilkan pesan keberhasilan atau kegagalan penarikan tunai |
| 13. | Aktor | Memilih menu "Transfer Dana"     | Sistem | Meminta aktor untuk memasukkan username penerima dan jumlah transfer, lalu mengecek apakah pengguna penerima ada dan saldo mencukupi untuk melakukan transfer |
| 14. | Aktor | Memasukkan username penerima dan jumlah transfer | Sistem | - Jika pengguna penerima ditemukan:                                                                               |
|     |       |                                                  |        |   - Mengurangi jumlah transfer dari saldo pengguna pengirim                                                      |
|     |       |                                                  |        |   - Menambahkan jumlah transfer ke saldo pengguna penerima                                                        |
|     |       |                                                  |        |   - Memperbarui riwayat transaksi pengirim dengan informasi transfer                                               |
|     |       |                                                  |        |   - Menampilkan pesan keberhasilan transfer                                                                         |
|     |       |                                                  |        | - Jika pengguna penerima tidak ditemukan:                                                                         |
|     |       |                                                  |        |   - Menampilkan pesan "Username penerima tidak ditemukan!"                                                        |
|     |       |                                                  |        | - Jika saldo pengguna pengirim tidak mencukupi:                                                                   |
|     |       |                                                  |        |   - Menampilkan pesan "Saldo Anda tidak mencukupi!"                                                               |
| 15. | Aktor | Memilih menu "Riwayat Transaksi"  | Sistem | Menampilkan riwayat transaksi pengguna jika ada, atau pesan bahwa belum ada riwayat transaksi jika tidak ada            |
| 16. | Aktor | Memilih menu "Keluar"             | Sistem | Menampilkan pesan terima kasih dan mengakhiri program                                                                 |



# No.3
**Penerapan SOLID Design Principle**

# No.4 
**Design Pattern**



# No.5
**Konektivitas ke database**

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UAS/5._Database.gif)

Konektivitas menggunakan Firebase sebagai layanan database. Kode tersebut menghubungkan ke Firebase Realtime Database menggunakan Firebase Unity SDK.

Pada metode InitializeFirebase(), koneksi ke Firebase diinisialisasi dengan mengatur instance auth dan DBreference sesuai dengan instance default dari Firebase Authentication dan Firebase Realtime Database.

Selanjutnya, metode-metode seperti Login(), Register(), UpdateUsernameAuth(), UpdateUsernameDatabase(), UpdateSaldo(), LoadUserData(), TransferSaldo(), dan TopUpSaldo() dan lain lain berinteraksi dengan Firebase Authentication dan Firebase Realtime Database untuk melakukan operasi seperti login, register, memperbarui profil pengguna, menyimpan dan memuat data saldo, dan mentransfer saldo.

Dengan demikian, program tersebut menggunakan Firebase untuk menyimpan data pengguna dan melakukan operasi yang berkaitan dengan otentikasi pengguna dan penyimpanan data di database Firebase.

# No.6
**Pembuatan web service dan setiap operasi CRUD** 

Web service dan CRUD karena disini saya menggunakan sdk firebase jadi untuk restful apinya sudah tertanam secara default pada package/library yang dipasang

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UAS/6._Firebase.gif)

CRUD upload, load, edit data

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UAS/6._CRUD_.gif)

Firebase SDK menyediakan antarmuka yang nyaman dan terintegrasi dengan Firebase untuk melakukan operasi CRUD (Create, Read, Update, Delete) pada database Firebase secara langsung dari aplikasi Anda. Dengan menggunakan SDK Firebase, Anda dapat mengakses dan memanipulasi data di database Firebase tanpa perlu membangun atau mengelola web service sendiri.

Firebase SDK telah memperhatikan implementasi RESTful API dan melakukan abstraksi ke dalam fungsi-fungsi yang lebih tinggi untuk operasi yang umum dilakukan, seperti membuat pengguna baru, login, mengambil data, menambahkan data, memperbarui data, dan sebagainya. SDK menyembunyikan detail implementasi RESTful API dan menyediakan metode-metode yang mudah digunakan untuk berinteraksi dengan Firebase.

Jadi, SDK Firebase menyediakan antarmuka yang mudah digunakan untuk berkomunikasi dengan database Firebase menggunakan metode-metode yang sudah disediakan, tanpa perlu secara eksplisit membangun atau mengelola RESTful API sendiri. Ini memudahkan pengembangan aplikasi dan mengurangi kerumitan dalam melakukan operasi CRUD pada database Firebase.

# No.7
**Graphical User Interface (GUI) dari Dana**

# No.8 
**HTTP connection melalui GUI produk digital Dana**

# No.9
**Video Youtube**

Link Ytb: 

# No.10
**Machine Learning pada produk yang digunakan**

Mungkin untuk pengembangan produk digital yang saya buat kedepannya akan melibatkan penggunaan machine learning pada produk digital ini.
