# Nomor 1

Algoritma deskriptif:

1. Inisialisasi:
   - Buatlah sebuah list kosong bernama `users` untuk menyimpan data pengguna.
   - Tetapkan `currentUser` sebagai `null`.

2. Method `register`:
   - Terima parameter `user` yang merupakan objek dari kelas `User`.
   - Tambahkan `user` ke dalam list `users`.
   - Cetak pesan "Registrasi berhasil!".

3. Method `login`:
   - Terima parameter `username` dan `password` yang merupakan data login.
   - Lakukan perulangan untuk setiap `user` dalam list `users`.
   - Periksa apakah `username` dan `password` sesuai dengan data `user` saat ini.
   - Jika sesuai, tetapkan `currentUser` sebagai `user`, cetak pesan "Login berhasil!", dan kembalikan nilai `true`.
   - Jika tidak ada kecocokan ditemukan, kembalikan nilai `false`.

4. Method `startApp`:
   - Buat sebuah objek `Scanner` untuk menerima input dari pengguna.
   - Cetak pesan selamat datang di aplikasi.
   - Tampilkan menu utama dalam perulangan tak terbatas.
   - Minta pengguna memilih menu (1-3).
   - Gunakan pernyataan switch-case untuk menangani pilihan pengguna:
     - Case 1: Registrasi
       - Minta pengguna memasukkan username, password, dan nomor kontak.
       - Buat objek `User` baru dengan data yang dimasukkan pengguna.
       - Panggil method `register` dengan objek `User` baru sebagai parameter.
       - Break dari switch-case.
     - Case 2: Login
       - Minta pengguna memasukkan username dan password.
       - Panggil method `login` dengan username dan password sebagai parameter.
       - Jika login berhasil, panggil method `handleLoggedIn` untuk memproses menu setelah login.
       - Jika login gagal, cetak pesan kesalahan.
       - Break dari switch-case.
     - Case 3: Keluar
       - Cetak pesan terima kasih dan keluar dari aplikasi.
       - Return dari method `startApp`.

5. Method `handleLoggedIn`:
   - Buat objek `Scanner` untuk menerima input dari pengguna.
   - Cetak pesan selamat datang kepada pengguna saat ini.
   - Tampilkan menu utama setelah login dalam perulangan tak terbatas.
   - Minta pengguna memilih menu (1-6).
   - Gunakan pernyataan switch-case untuk menangani pilihan pengguna:
     - Case 1: Lihat Saldo
       - Cetak pesan dengan saldo pengguna saat ini.
       - Break dari switch-case.
     - Case 2: Top Up
       - Minta pengguna memasukkan jumlah top up.
       - Buat objek `TopUp` dan panggil method `topUp` dengan jumlah top up sebagai parameter.
       - Break dari switch-case.
     - Case 3: Tarik Tunai
       - Minta pengguna memasukkan jumlah tarik tunai.
       - Buat objek `TarikTunai` dan panggil method `tarik` dengan jumlah tarik tunai sebagai parameter.
       - Break dari switch-case.
     - Case 4: Transfer Dana
       - Minta pengguna memasukkan username penerima dan jumlah transfer.
       - Buat objek `TransferDana` dan panggil method `transfer` dengan username penerima dan jumlah transfer sebagai parameter.
       - Break dari switch-case.
     - Case 5: Logout
       - Tetapkan `currentUser` sebagai `null`.
       - Cetak pesan "Logout berhasil!".
       - Return dari method `handleLoggedIn`.
     - Case 6: Keluar
       - Cetak pesan terima kasih dan keluar dari aplikasi.
       - Return dari method `handleLoggedIn`.


Dalam keseluruhan program, terdapat banyak algoritma yang terlibat dalam mengatur logika bisnis dan interaksi antara class yang ada. Salah satu contoh penerapan algoritma dalam program tersebut adalah pada class TarikTunai. Pada metode tarik, terdapat algoritma yang memeriksa apakah saldo pengguna mencukupi untuk melakukan penarikan tunai. Selanjutnya Metode transfer dalam kelas TransferDana, algoritma ini memeriksa apakah saldo pengirim mencukupi untuk melakukan transfer dana. Kemudian metode topUp dalam kelas TopUp, algoritma ini menambahkan jumlah top-up ke saldo pengguna, dan riwayat transaksi diperbarui dengan entri "Top Up: +[amount]".
Semua algoritma ini berkontribusi dalam menjalankan fungsionalitas aplikasi secara tepat dan akurat.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/1._Algo_Math.gif)


# Nomor 2
Seperti yang telah dilampirkan pada nomor 1, Algoritma yang terdapat pada program seperti Algoritma pada Register, Login, StartApp, HandleLoggedIn. Dengan menggunakan algoritma-algoritma ini, program dapat melakukan registrasi pengguna, login, memproses berbagai tindakan seperti top-up, tarik tunai, transfer, dan menampilkan riwayat transaksi. Algoritma-algoritma tersebut membantu menjalankan logika bisnis yang ada dalam aplikasi Dana.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/2._Algoritma2.gif)

Algoritma yang sudah saya berikan di no. 1 kemudian diimplemetasikan ke dalam source code. Berikut adalah source code-nya:

[DanaApp.java](Program/DanaApp.java)

[Login.java](Program/Login.java)

[RiwayatTransaksi.java](Program/RiwayatTransaksi.java)

[TarikTunai.java](Program/TarikTunai.java)

[TopUp.java](Program/TopUp.java)

[TransferDana.java](Program/TransferDana.java)

[User.java](Program/User.java)


# Nomor 3
Dalam projek yang saya buat sudah terdapat implementasi dari oop yang sudah saya pelajari sebelumnya. Oop adalah paradigma pemrograman yang berfokus pada penggunaan objek dan kelas untuk merancang dan membangun aplikasi. 

Konsep Dasar OOP, yaitu ada objek, class, attribute, dan methode.

- Objek : Objek merupakan segala sesuatu yang ada di dunia nyata. Setiap objek memiliki atribut dan bisa melakukan tindakan (method). Objek adalah bentuk atau contoh nyata dari class.

- Class : Class merupakan template/kerangka untuk membuat objek. Class menggambarkan apa yang objek dapat lakukan dan bagaimana objek tersebut berinteraksi dengan objek lain. Class berupa kumpulan atas definisi data dan fungsi dalam suatu unit untuk suatu tujuan tertentu.
Contohnya ada class of cat atau kelas kucing yang mendefinisikan suatu unit yang terdiri atas definisi-definisi data dan fungsi-fungsi yang menunjuk pada beberapa macam perilaku dari kucing.


- Attribute : Attribute atau property adalah data yang membedakan antara objek satu dengan objek yang lainnya.
- Method : Method atau disebut juga tingkah laku adalah hal-hal yang bisa dilakukan objek dari suatu class.


Di dalam OOP juga ada 4 pilar, yaitu: Abstraction, Encapsulation, Inheritance, dan Polymorphism.


![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/3._Konsep_OOP.gif)


Mengapa program berbasis OOP banyak digunakan di industri? 
Yang saya ketahui dari dengan mencari informasi di internet OOP banyak digunakan di industri karena keuntungannya yaitu reusability dimana developer bisa menggunakan ulang bagian-bagian atau class yang sudah dibuat. Class tersebut bisa diubah dan digunakan untuk proyek sejenis. Kemudian paralel development dimana developer yang bekerja secara tim dapat membangun class sendiri lalu nanti bisa digabung dengan beberapa class berbeda. Kemudian juga dalam Sistem OOP bisa ditingkatkan dengan mudah dan biaya perawatannya pun dinilai rendah.
Reusabilitas yaitu kode bisa digunakan berkali², misalkan:
Reusabilitas pada program Aplikasi Dana terdapat pada class-class yang berkaitan dengan pengguna (`User`) dan transaksi (`Transaksi` dan turunannya). Misalnya, class `User` dapat digunakan berkali-kali untuk membuat objek pengguna dengan properti seperti username, password, saldo, nomor kontak, dan riwayat transaksi. Class ini bisa digunakan di berbagai aplikasi yang membutuhkan fitur pengguna.
Selain itu, class-class turunan dari `Transaksi`, yaitu `TransferDana`, `TarikTunai`, dan `TopUp`, juga dapat digunakan kembali di aplikasi lain yang membutuhkan fitur serupa. Misalnya, class `TransferDana` dapat digunakan untuk melakukan transfer dana antar pengguna tanpa perlu membuat method seperti `transfer(User receiver, double amount)` berulang kali. Begitu juga dengan class `TarikTunai` yang dapat digunakan untuk melakukan penarikan tunai tanpa perlu membuat method seperti `tarik(double amount)` berkali-kali. Class `TopUp` juga dapat digunakan untuk melakukan top up saldo tanpa perlu membuat method seperti `topUp(double amount)` secara berulang.
Dengan menggunakan komponen-komponen di atas, kita dapat dengan mudah mengintegrasikan fitur-fitur ini ke dalam aplikasi lain yang memerlukan fungsionalitas serupa, tanpa perlu menulis kode yang serupa atau identik. Sehingga, kita dapat menghemat waktu dan usaha dalam mengembangkan aplikasi baru dengan fitur yang telah ada.

# Nomor 4
Encapsulation digunakan untuk menyembunyikan implementasi detail dari suatu objek atau kelas, dan hanya mengungkapkan fungsi-fungsi publik untuk interaksi dengan objek atau kelas tersebut. Dalam enkapsulasi, data dan fungsi terkait dikemas bersama dalam sebuah unit yang disebut kelas. Tujuan utama dari enkapsulasi adalah untuk menjaga integritas data dan mencegah penggunaan yang salah atau tidak sah dari data atau fungsi tersebut. Hal ini memungkinkan kontrol yang lebih baik terhadap pengaksesan dan manipulasi data dalam program. Contohnya seperti data-data atribut dalam class User, Transaksi, Riwayat Transaksi tentunya data-data tersebut tidak bisa lagi diubah.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/4._Encapsulation.gif)


# Nomor 5
Class Transaksi menjadi sebuah Abstraction sehingga kita dapat membuat program yang lebih modular dan mudah diatur dengan mengelompokkan konten-konten menjadi satu kelas abstrak yang memiliki properti dan metode yang sama. Class Transaksi nantinya berfungsi sebagai wadah yang menampung data dari childnya. Kita juga dapat menambahkan perilaku atau properti khusus pada setiap kelas turunan sesuai dengan kebutuhan aplikasi. Kelas turunan dari Transaksi yaitu class TransferDana, TarikTunai, dan TopUp, nantinya akan mengambil semua method dan atribut dalam class parentnnya dan akan memiliki sifat unik masing-masing dalam class childnya. Ini memungkinkan kita untuk menerapkan pola desain seperti Polymorphism dan menyediakan fleksibilitas dalam mengelola objek dengan tipe yang berbeda secara generik melalui kontrak abstraksi yang diberikan oleh class abstrak.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/5._Abstract.gif)


# Nomor 6

Inheritance atau pewarisan dalam OOP adalah konsep yang memungkinkan kita untuk membuat hubungan hierarki antara kelas-kelas. Di dalam inheritance itu ada superclass dan subclass, yang di mana superclass adalah kelas dasar dari subclass atau dimisalkan superclass adalah kelas induk dari turunan-turunannya (subclass).

Polymorphism adalah konsep dalam OOP yang memungkinkan objek dengan jenis yang berbeda untuk diakses dan diproses menggunakan cara yang sama. 

Penggunaan Inheritance dan Polymorphism dapat ditemukan dalam program di atas melalui hubungan antara class-class yang saling terkait dan kemampuan untuk menggunakan objek turunan sebagai objek superclass. Disini saya menggunakan Inheritance pada Class Transaksi menjadi parentnya karena disini memuat atribut-atribut yang bisa diturunkan ke childnya nanti. Class TransferDana, TarikTunai, dan TopUp, menjadi childnya dan akan mengextends parentnya untuk mendapatkan sifat-sifat yang dimiliki oleh parentnya agar bisa digunakan oleh childnya. Sedangkan untuk Polymorphism yaitu pada method abstrak updateRiwayatTransaksi yang nantinya akan di override oleh childnya sehingga memiliki sifat khusus dan unik.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/6._Inheritance___Polymorphism.gif)


# Nomor 7

Proses bisnis atau kumpulan use case yang dapat diimplementasikan dalam program Aplikasi Dana dapat meliputi:

1. Registrasi Pengguna:
   - Pengguna dapat mendaftar sebagai anggota aplikasi dengan memasukkan informasi pribadi seperti username, password, dan nomor kontak.
   - Data pengguna baru akan disimpan dalam sistem.

2. Login:
   - Pengguna yang telah terdaftar dapat melakukan login ke dalam akun mereka dengan menggunakan username dan password yang valid.
   - Setelah login berhasil, pengguna dapat mengakses fitur-fitur yang tersedia dalam aplikasi.

3. Lihat Saldo:
   - Pengguna dapat melihat saldo terkini dalam akun mereka.
   - Saldo ini mencerminkan jumlah dana yang tersedia untuk digunakan dalam aplikasi.

4. Top Up:
   - Pengguna dapat melakukan penambahan dana ke dalam akun mereka melalui proses top up.
   - Pengguna akan diminta untuk memasukkan jumlah dana yang ingin ditambahkan.
   - Setelah berhasil, jumlah dana akan ditambahkan ke saldo akun pengguna.

5. Tarik Tunai:
   - Pengguna dapat melakukan penarikan dana dari akun mereka melalui proses tarik tunai.
   - Pengguna akan diminta untuk memasukkan jumlah dana yang ingin ditarik.
   - Jika saldo mencukupi, jumlah dana akan dikurangi dari saldo akun pengguna.

6. Transfer Dana:
   - Pengguna dapat mentransfer dana ke pengguna lain melalui fitur transfer dana.
   - Pengguna akan diminta untuk memasukkan username penerima dan jumlah dana yang ingin ditransfer.
   - Jika saldo mencukupi, jumlah dana akan dikurangi dari saldo pengirim dan ditambahkan ke saldo penerima.

7. Logout:
   - Pengguna dapat logout dari akun mereka untuk keluar dari aplikasi.
   - Setelah logout, pengguna perlu melakukan login kembali untuk mengakses fitur-fitur aplikasi.

Dalam implementasi OOP pada program Aplikasi Dana, setiap use case direpresentasikan sebagai metode atau fungsi yang dijalankan pada objek-objek yang relevan. Objek-objek seperti User, DanaApp, Transaksi, TransferDana, TarikTunai, TopUp, dan RiwayatTransaksi digunakan untuk menyimpan data dan logika bisnis yang terkait dengan setiap use case.


# Nomor 8
Diagram class menggambarkan struktur class dalam program Aplikasi Dana
![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/8._Diagram_Class_Dana_App.png)

Update memakai Mermaid.js
![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/8._Diagram_Class_Mermaid.js_update_1.png)

Tabel Use Case menyediakan ringkasan dari use case dan deskripsi mereka dalam sistem.

Use Case Table User

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/8._Use_Case_Table_Dana-User_1_.png)

Use Case Table Admin

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/8._Use_Case_Table_Dana-Admin.png)


# Nomor 9
https://youtu.be/qbHCYRHCHHU


# Nomor 10
Pada projek saya tampilannya menggunakan cmd seperti dibawah ini.

![](https://gitlab.com/kireinaar1618/tugas-besar-prak-pbo/-/raw/main/Jawaban%20UTS/10._Inovasi_UX.gif)
